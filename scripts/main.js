function moduleHome() { //creates a function 
  var x = document.getElementById("module-info"); //creates a var that links to the id in HTML
  if (x.style.display === "none") { //if statement that displays the cotent if clicked
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

fetch('module-1.json')  //fetches the module-1 json files 
          .then(function (response) {
          return response.json();
          })
          .then(function (data) {
          appendData(data);
          })         
fetch('module-2.json') //fetches the module-2 json files
          .then(function (response) {
              return response.json();
          })
          .then(function (data) {
              appendData(data);
          })
fetch('module-3.json') //fetches the module-3 json files 
          .then(function (response) {
              return response.json();
          })
          .then(function (data) {
              appendData(data);
          })
    
      function appendData(data) { //appends the data from the json files.
          var mainContainer = document.getElementById("module-info");  //creates a var that links to the id in HTML
          for (var i = 0; i < data.length; i++) { //appends each of the json files using their naming conventions and attaches to a div.
              var div = document.createElement("div");  //creates a var that links to the id in HTML
              div.innerHTML = data[i].Name + ' is a ' + data[i].Course + ' has learning outcomes ' + data[i].Learning_outcomes + '. The degrees have modules that have learning outcomes ' + data[i].Learning_outcomes_mod + '. Number of hours spent on the module ' + data[i].Num_of_hours + ' and the credits it is worth ' + data[i].Num_of_credits + '. The module timelot is on ' + data[i].Timeslot;
              mainContainer.appendChild(div); //All the data from the json files is attached including the text

          }
        }