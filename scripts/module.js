function moduleDropdown() { //creates a function 
    var x = document.getElementById("moduleData"); //creates a var that links to the id in HTML
    if (x.style.display === "none") { //if statement that displays the cotent if clicked
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
 
fetch('module-1.json')  //fetches the module-1 json files 
            .then(function (response) {
            return response.json();
            })
            .then(function (data) {
            appendData(data);
            })         
fetch('module-2.json') //fetches the module-2 json files
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                appendData(data);
            })
fetch('module-3.json') //fetches the module-3 json files 
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                appendData(data);
            })
      
        function appendData(data) { //appends the data from the json files.
            var mainContainer = document.getElementById("moduleData");  //creates a var that links to the id in HTML
            for (var i = 0; i < data.length; i++) { //appends each of the json files using their naming conventions and attaches to a div.
                var div = document.createElement("div");  //creates a var that links to the id in HTML
                div.innerHTML = 'Degree ID: ' + data[i].Identifcation_code + '<br>' +  'Name: ' + data[i].Name + '<br>' + 'Course: ' + data[i].Course + '<br>' + 'Learning outcome: ' + data[i].Learning_outcomes+ '<br>' + 'Exit awards: ' + data[i].Exit_awards + '<br>' + 'Module ID: ' + data[i].Identifcation_code_mod + '<br>' +  'Module name: ' + data[i].Module + '<br>' + 'Module Learning outcome:' + data[i].Learning_outcomes_mod + '<br>' + 'Number of Hours:' + data[i].Num_of_hours + '<br>' + 'Number of credits:' + data[i].Num_of_credits + '<br>' + 'Timeslot:' + data[i].Timeslot;
                mainContainer.appendChild(div); //All the data from the json files is attached including the text

            }
          }